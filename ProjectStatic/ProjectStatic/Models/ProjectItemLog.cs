﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace ProjectStatic.Models
{
    [Table("")]
    public class ProjectItemLog : BaseModel
    {
        public ProjectItemLog()
        {
            this.No = 0;
            this.Note1 = "";
            this.Note2 = "";
        }

        [Column("次数")]
        public double No { get; set; }
        [Column("分项内容")]
        public string ItemName { get; set; }
        [Column("点数")]
        public double Points { get; set; }
        [Column("日期")]
        public string Date { get; set; }
        [Column("备注1")]
        public string Note1 { get; set; }
        [Column("备注2")]
        public string Note2 { get; set; }
    }
}