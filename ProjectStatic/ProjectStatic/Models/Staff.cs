﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
namespace ProjectStatic.Models
{

   

    [Table("staffs")]
    public class Staff : BaseModel
    {
        [Column("姓名")]
        [Display(Name = "项目编号")]
        public string Name { get; set; }

        [Column("班组长")]
        public string TeamLeader { get; set; }

        [Column("项目经理")]
        public string PrjManager { get; set; }

        [Column("工作状态")]
        public string Status { get; set; }

    }
}