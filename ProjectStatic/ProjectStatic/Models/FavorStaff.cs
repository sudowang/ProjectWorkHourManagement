﻿using System.ComponentModel.DataAnnotations.Schema;
namespace ProjectStatic.Models
{
    [Table("FavorStaff")]
    public class FavorStaff : BaseModel
    {
        [Column("Name")]
        public string Name { get; set; }

        [Column("StaffName")]
        public string StaffName { get; set; }

        [Column("Note")]
        public string Note { get; set; }
    }

}