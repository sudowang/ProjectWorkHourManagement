﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace ProjectStatic.Models
{
    [Table("users")]
    public class UserP : BaseModel
    {
        [Column("姓名")]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Column("密码")]
        public string Psw { get; set; }

        [Column("权限")]
        public string Authority { get; set; }

    }
}