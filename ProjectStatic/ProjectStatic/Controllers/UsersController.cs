﻿using Newtonsoft.Json.Linq;
using ProjectStatic.Models;
using ProjectStatic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjectStatic.Controllers
{
    public class UsersController : ApiController
    {
        // GET api/users
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/users/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/users
        public bool Post([FromBody]string value)
        {
            JObject jsonData = JObject.Parse(value);
            string name = jsonData["name"].ToString();
            string psw = jsonData["psw"].ToString();
            UserP u = new UserP() { Name = name };
            DatasDb<UserP> dbs = new DatasDb<UserP>();
            var list = dbs.QueryDatas(u.GenerateFindSqlStr());
            if (list.Count == 0 || list[0].Psw != psw)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        // PUT api/users/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/users/5
        public void Delete(int id)
        {
        }
    }
}
