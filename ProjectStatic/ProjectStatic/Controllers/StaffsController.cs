﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectStatic.Models;
using ProjectStatic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjectStatic.Controllers
{
    public class StaffsController : ApiController
    {
        // GET api/staffs
        public string Get()
        {
            DatasDb<Staff> db = new DatasDb<Staff>();
            var list = db.GetDatas().Where(p => p.Status == "在职");
            List<Staff> staffs = new List<Staff>();
            //foreach (var item in list)
            //{
            //    var staff = new Staff()
            //    {
            //        Name = item.Key,
            //        PrjManager = item.First().PrjManager,
            //        Status = item.First().Status,
            //        TeamLeader = item.First().TeamLeader
            //    };
            //    staffs.Add(staff);
            //}

            return JsonConvert.SerializeObject(list);
        }

        // GET api/staffs/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/staffs
        public void Post([FromBody]string value)
        {
        }

        // PUT api/staffs/5
        public void Put([FromBody]string value)
        {
            JObject jsonData = JObject.Parse(value);
            bool IsAdd = jsonData["IsAdd"].ToObject<bool>();
            Staff OldValue = jsonData["OldValue"].ToObject<Staff>();
            Staff NewValue = jsonData["NewValue"].ToObject<Staff>();
            string sql = "";
            List<string> list = new List<string>();
            if (IsAdd == true)
            {
                sql = string.Format("delete from staffs where 姓名='{0}'", "projects", NewValue.Name);
                list.Add(sql);
                list.Add(NewValue.GenerateInsertSqlStr());
                FavorStaff temp = new FavorStaff()
                {
                    StaffName = NewValue.Name,
                    Name = jsonData["name"].ToString()
                };
                list.Add(temp.GenerateInsertSqlStr());
            }
            else
            {
                sql = string.Format("update staffs set 姓名='{0}',班组长='{1}',项目经理='{2}' where 姓名='{3}'",
                    NewValue.Name, NewValue.TeamLeader, NewValue.PrjManager, OldValue.Name);
                list.Add(sql);
                if (NewValue.Name != OldValue.Name)
                {
                    sql = string.Format("update employees set 姓名='{0}' where 姓名='{1}'",
                        NewValue.Name, OldValue.Name);
                    list.Add(sql);
                    sql = string.Format("update employees set 班组长='{0}' where 班组长='{1}'",
                     NewValue.Name, OldValue.Name);
                    list.Add(sql);
                    sql = string.Format("update projectlog set 班组长='{0}' where 班组长='{1}'",
                        NewValue.Name, OldValue.Name);
                    list.Add(sql);
                    sql = string.Format("update salary set 姓名='{0}' where 姓名='{1}'",
                        NewValue.Name, OldValue.Name);
                    list.Add(sql);
                    sql = string.Format("update FavorStaff set Name='{0}' where Name='{1}'",
                       NewValue.Name, OldValue.Name);
                    list.Add(sql);
                    sql = string.Format("update FavorStaff set StaffName='{0}' where StaffName='{1}'",
                 NewValue.Name, OldValue.Name);
                    list.Add(sql);
                }
            }
            DbHelperSQL.ExecuteSqlTran(list);
        }

        // DELETE api/staffs/5
        public void Delete(int id)
        {
        }
    }
}
