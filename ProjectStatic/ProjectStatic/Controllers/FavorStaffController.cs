﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectStatic.Models;
using ProjectStatic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjectStatic.Controllers
{
    public class FavorStaffController : ApiController
    {
        // GET api/favorstaff
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/favorstaff/5
        public string Get(string name)
        {
            string sql = string.Format("select * from staffs,FavorStaff" +
          " where FavorStaff.Name='{0}' and FavorStaff.StaffName=staffs.姓名", name);
            DatasDb<Staff> dbs = new DatasDb<Staff>();
            List<Staff> prjs = dbs.QueryDatas(sql);
            return JsonConvert.SerializeObject(prjs);
        }

        // POST api/favorstaff
        public void Post([FromBody]string value)
        {
            JObject jsonData = JObject.Parse(value);
            string name = jsonData["name"].ToString();
            string[] staffList = jsonData["staffList"].ToObject<string[]>();
            List<string> sqlList = new List<string>();
            foreach (string str in staffList)
            {
                string sql = string.Format("delete from FavorStaff where Name='{0}' and StaffName ='{1}'", name, str);
                sqlList.Add(sql);
            }
            DbHelperSQL.ExecuteSqlTran(sqlList);
        }

        // PUT api/favorstaff/5
        public bool Put([FromBody]string value)
        {
            JObject jsonData = JObject.Parse(value);
            string name = jsonData["name"].ToString();
            string[] staffList = jsonData["staffList"].ToObject<string[]>();
            List<string> sqlList = new List<string>();
            foreach (string str in staffList)
            {
                FavorStaff prj = new FavorStaff()
                {
                    StaffName = str,
                    Name = name
                };
                string sql = string.Format("delete from FavorStaff where Name='{0}' and StaffName ='{1}'", name, str);
                sqlList.Add(sql);
                sql = prj.GenerateInsertSqlStr();
                sqlList.Add(sql);
            }

            DbHelperSQL.ExecuteSqlTran(sqlList);
            return true;
        }

        // DELETE api/favorstaff/5
        public void Delete(int id)
        {
        }
    }
}
