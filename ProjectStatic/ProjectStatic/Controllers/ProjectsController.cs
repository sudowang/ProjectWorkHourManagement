﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectStatic.Models;
using ProjectStatic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjectStatic.Controllers
{
    public class ProjectsController : ApiController
    {
        // GET api/projects
        public string Get()
        {
            DatasDb<Project> db = new DatasDb<Project>();
            var list = db.GetDatas().Where(p => p.PrjType != "监测" && p.PrjType != "数字").OrderByDescending(p => p.PrjId);
            return JsonConvert.SerializeObject(list);
        }

        // GET api/projects/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/projects
        public void Post([FromBody]string value)
        {

        }

        // PUT api/projects/5
        public void Put([FromBody]string value)
        {
            JObject jsonData = JObject.Parse(value);
           
            bool IsAdd = jsonData["IsAdd"].ToObject<bool>();
            Project OldValue = jsonData["OldValue"].ToObject<Project>();
            Project NewValue = jsonData["NewValue"].ToObject<Project>();
            string sql = "";
            List<string> list = new List<string>();
            if (IsAdd == true)
            {
               
                sql = string.Format("delete from {0} where 项目编号='{1}' and 项目名称='{2}'", "projects", NewValue.PrjId, NewValue.PrjName);
                list.Add(sql);
                list.Add(NewValue.GenerateInsertSqlStr());
                sql = string.Format("select *  into [{0}] from project_template", NewValue.PrjId);
                list.Add(sql);
                FavorProject prj = new FavorProject()
                {
                    FavorPrjId = NewValue.PrjId,
                    Name = jsonData["name"].ToString()
                };
                list.Add(prj.GenerateInsertSqlStr());
            }
            else
            {
                sql = string.Format("update [projects] set [项目编号]='{0}',[项目名称]='{1}',[项目经理]='{2}',[班组长]='{3}'" +
                    " where [项目编号]='{4}'", NewValue.PrjId, NewValue.PrjName,NewValue.PrjManager,NewValue.PrjTeamLeader
                    ,OldValue.PrjId);
                list.Add(sql);
                if (NewValue.PrjId != OldValue.PrjId)
                {
                    sql = string.Format("EXEC sp_rename '{0}', '{1}'", OldValue.PrjId, NewValue.PrjId);
                    list.Add(sql);
                    sql = string.Format("update projectlog set 项目编号='{0}' where 项目编号='{1}'", NewValue.PrjId, OldValue.PrjId);
                    list.Add(sql);
                    sql = string.Format("update FavorProject set FavorPrjId='{0}' where FavorPrjId='{1}'", NewValue.PrjId, OldValue.PrjId);
                    list.Add(sql);
                }
            }
            DbHelperSQL.ExecuteSqlTran(list);
           
        }

        // DELETE api/projects/5
        public void Delete(int id)
        {
        }
    }
}
