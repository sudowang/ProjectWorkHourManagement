/**
 * Module dependencies.
 */

apiUrl = " http://61.129.33.248:8084/PrjStatic/api";
//apiUrl = " http://localhost:52482/api";

var express = require('express');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
//app.set('views', __dirname + '/views');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
require('./route.js')(app);

// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}


http.createServer(app).listen(app.get('port'), function() {
	console.log('Express server listening on port ' + app.get('port'));
});