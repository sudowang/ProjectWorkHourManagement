define(function(require, exports, module) {
	module.exports = function() {
		'use strict'; /* Directives */
		var app = angular.module('ProjectManage.directives', []);

		require('./autoLogIn.js')();
		require('./navHeader.js')();
		require('./removeAnimate.js')();
	}
});