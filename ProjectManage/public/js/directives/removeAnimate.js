define(function(require, exports, module) {
	module.exports = function() {
		var app = angular.module('ProjectManage.directives');
		app.directive('removeAnimate', function() {
			// Runs during compile
			return {
				link: function($scope, iElm, iAttrs) {
					var md = new MobileDetect(window.navigator.userAgent);
					if (md.mobile()) {
						//alert(md.os() + " " + document.body.scrollWidth);
						var fontStyle = document.createElement("style");
						fontStyle.appendChild(document.createTextNode("@media  (max-device-width: 768px) {.navbar-nav > li a {font-size: 32px;}}"));
						fontStyle.appendChild(document.createTextNode(".view-animate.ng-leave{top:90px;}"));
						fontStyle.appendChild(document.createTextNode("@media  (min-width: 1080px) {@-ms-viewport {width: 1080px !important;}}"));
						document.getElementsByTagName("head")[0].appendChild(fontStyle);
					}
				}
			};
		});
	}
});