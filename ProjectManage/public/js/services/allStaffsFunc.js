define(function(require, exports, module) {
	module.exports = function() {
		'use strict';

		/* Services */


		// Demonstrate how to register services
		// In this case it is a simple value service.
		var app = angular.module('ProjectManage.services');
		app.factory('allStaffsFunc', ['$http', '$rootScope', 'myStaffsFunc',
			function($http, $rootScope, myStaffsFunc) {
				function filterFavor() {
					angular.forEach($rootScope.staffs, function(value) {
						value.isFavor = false;
						for (var i = 0; i < $rootScope.mystaffs.length; i++) {
							if ($rootScope.mystaffs[i].Name === value.Name) {
								value.isFavor = true;
								break;
							}
						}
					});
				}
				return {
					getAllStaffs: function(callback) {
						if ($rootScope.staffs.length == 0) {
							myStaffsFunc.getMyStaffs(function() {
								$http({
									method: "GET",
									url: "/staffs"
								}).success(function(data) {
									$rootScope.staffs = data;
									filterFavor();
									if (callback != undefined) {
										callback();
									}
								});
							});
						} else {
							filterFavor();
							if (callback != undefined) {
								callback();
							}
						}

					},
					addStaff: function(data, callback) {
						$http({
							'method': "PUT",
							'url': "/staffs",
							'data': data
						}).success(function(data) {
							if (callback != undefined) {
								callback();
							}
						});
					}
				};
			}
		])

	}
});