define(function(require, exports, module) {
	module.exports = function() {
		'use strict';

		/* Services */


		// Demonstrate how to register services
		// In this case it is a simple value service.
		var app = angular.module('ProjectManage.services');
		app.factory('allProjectsFunc', ['$http', '$rootScope', 'myProjectsFunc',
			function($http, $rootScope, myProjectsFunc) {
				function filterFavor() {
					angular.forEach($rootScope.projects, function(value) {
						value.isFavor = false;
						for (var i = 0; i < $rootScope.myprojects.length; i++) {
							if ($rootScope.myprojects[i].PrjId === value.PrjId) {
								value.isFavor = true;
								break;
							}
						}
					});
				}
				return {
					getAllProjects: function(callback) {
						if ($rootScope.projects.length == 0) {
							myProjectsFunc.getMyProjects(function() {
								$http({
									method: "GET",
									url: "/projects"
								}).success(function(data) {
									$rootScope.projects = data;
									filterFavor();

									if (callback != undefined) {
										callback();
									}
								});
							});
						} else {
							filterFavor();
							if (callback != undefined) {
								callback();
							}
						}

					},
					addProject: function(data, callback) {
						$http({
							'method': "PUT",
							'url': "/projects",
							'data':data
						}).success(function(data) {
							if (callback != undefined) {
								callback();
							}
						});
					}
				};
			}
		])

	}
});