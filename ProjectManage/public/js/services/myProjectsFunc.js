define(function(require, exports, module) {
	module.exports = function() {
		'use strict';

		/* Services */


		// Demonstrate how to register services
		// In this case it is a simple value service.
		var app = angular.module('ProjectManage.services');
		app.factory('myProjectsFunc', ['$rootScope', '$http', function($rootScope, $http) {
			return {
				getMyProjects: function(callback) {
					if ($rootScope.myprojects.length == 0) {
						$http({
							url: '/favoriteProjects/' + $rootScope.user.name,
							method: 'get'
						}).success(function(data) {
							$rootScope.myprojects = angular.fromJson(data);
							angular.forEach($rootScope.myprojects, function(prj) {
								prj.checked = false;
							});
							if (callback != undefined) {
								callback();
							}
						});
					} else {
						if (callback != undefined) {
							callback();
						}
					}
				},
				addMyProject: function(prj, callback) {
					prj.checked = true;
					$rootScope.myprojects.push(prj);
					if (callback != undefined) {
						callback();
					}
				},
				removeMyProject: function(prj, callback) {
					prj.checked = true;
					for (var i = 0; i < $rootScope.myprojects.length; i++) {
						if ($rootScope.myprojects[i].PrjId === prj.PrjId) {
							$rootScope.myprojects.splice(i, 1);
							break;
						}
					}
					if (callback != undefined) {
						callback();
					}


				},
				addFavorProjects: function(prjList) {
					if (prjList.length > 0) {
						$http({
							url: '/favoriteProjects',
							method: 'put',
							data: {
								'name': $rootScope.user.name,
								'prjList': prjList
							}
						}).success(function(data) {});
					}
				},
				removeFavorProjects: function(prjList) {
					if (prjList.length > 0) {
						$http({
							url: '/favoriteProjects',
							method: 'post',
							data: {
								'name': $rootScope.user.name,
								'prjList': prjList
							}
						}).success(function(data) {});
					}
				}
			}
		}])
	}
});