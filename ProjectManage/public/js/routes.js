define(function(require, exports, module) {
	module.exports = function() {
		var app = angular.module('ProjectManage.routes', ['ngRoute']);
		app.config(['$routeProvider', '$locationProvider',
			function($routeProvider, $locationProvider) {
				$routeProvider.when('/todoproject', {
					templateUrl: 'partials/todoprojects.html',
					controller: 'todoprojectCtrl'
				});
				$routeProvider.when('/todostaff', {
					templateUrl: 'partials/todostaff.html',
					controller: 'todostaffCtrl'
				});
				$routeProvider.when('/home', {
					redirectTo: '/myprojects'
				});
				$routeProvider.when('/login', {
					templateUrl: 'partials/LogIn.html',
					controller: 'loginCtrl'
				});
				$routeProvider.when('/allprojects', {
					templateUrl: 'partials/projects.html',
					controller: 'projectsCtrl'
				});
				$routeProvider.when('/myprojects', {
					templateUrl: 'partials/home.html',
					controller: 'homeCtrl'
				});
				$routeProvider.when('/allstaffs', {
					templateUrl: 'partials/staffs.html',
					controller: 'staffsCtrl'
				});
				$routeProvider.when('/mystaffs', {
					templateUrl: 'partials/mystaffs.html',
					controller: 'mystaffsCtrl'
				});
				$routeProvider.when('/success', {
					templateUrl: 'partials/success.html',
					controller: 'successCtrl'
				});
				$routeProvider.when('/addproject', {
					templateUrl: 'partials/addproject.html',
					controller: 'addprojectCtrl'
				});
				$routeProvider.when('/addstaff', {
					templateUrl: 'partials/addstaff.html',
					controller: 'addstaffCtrl'
				});


				$routeProvider.otherwise({
					redirectTo: '/home'
				});
				$locationProvider.html5Mode(true);
			}
		]);
	}
});