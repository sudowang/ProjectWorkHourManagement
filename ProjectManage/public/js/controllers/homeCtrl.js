define(function(require, exports, module) {
	module.exports = function() {
		var app = angular.module('ProjectManage.controllers');
		app.controller('homeCtrl', ['$rootScope', '$scope', '$location', '$http', 'myProjectsFunc',
			function($rootScope, $scope, $location, $http, myProjectsFunc) {
				$scope.selectedCount = 0;


				function GetSelectedPrjCount() {
					// body...
					if ($rootScope.myprojects == undefined) return 0;
					var count = 0;
					angular.forEach($rootScope.myprojects, function(prj, key) {
						// body...
						if (prj.checked === true) {
							count++;
						}
					})
					$scope.selectedCount = count;
				}
				//获取我的收藏工程
				myProjectsFunc.getMyProjects(GetSelectedPrjCount);


				$scope.prjChecked = function(prj) {
					prj.checked = !prj.checked;
					if (prj.checked === true) {
						$scope.selectedCount++;
					} else {
						$scope.selectedCount--;
					}
				};
				$scope.doProject = function() {
					$rootScope.todoprojects = [];
					for (var i = 0; i < $rootScope.myprojects.length; i++) {
						if ($rootScope.myprojects[i].checked == true) {
							$rootScope.todoprojects.push({
								'PrjId': $rootScope.myprojects[i].PrjId,
								'PrjName': $rootScope.myprojects[i].PrjName,
								'Items': [],
								'Points': 0,
								'Hours': 0,
								'Date': '2000-1-1'
							});
						}
					}

					var count = $rootScope.todostaffs.length;
					if (count > 0) {
						localStorage.setItem("todostaffs", JSON.stringify($rootScope.todostaffs));
						localStorage.setItem("todoprojects", JSON.stringify($rootScope.todoprojects));
						$location.path('/todoproject').search({
							'Index': 0
						});
					} else {
						$location.path('/mystaffs');
					}


					//$location.path(url);
				}

				$scope.deleteProject = function() {
					var toBeDelete = [];
					for (var i = $rootScope.myprojects.length - 1; i >= 0; i--) {
						if ($rootScope.myprojects[i].checked == true) {
							toBeDelete.push($rootScope.myprojects[i].PrjId);
							$rootScope.myprojects.splice(i, 1);
							$scope.selectedCount--;
						}
					}
					myProjectsFunc.removeFavorProjects(toBeDelete);
				}
				$scope.selectAll = function() {
					$scope.selectedCount = 0;
					angular.forEach($rootScope.myprojects, function(prj, index) {
						prj.checked = true;
						$scope.selectedCount++;
					});

				};

				$scope.editproject = function() {
					for (var i = 0; i < $rootScope.myprojects.length; i++) {
						if ($rootScope.myprojects[i].checked === true) {
							var prj = $rootScope.myprojects[i];
							localStorage.setItem('prj', JSON.stringify(prj));
							$location.path('/addproject');
							break;
						}
					}

				}


			}
		]);
	};
});