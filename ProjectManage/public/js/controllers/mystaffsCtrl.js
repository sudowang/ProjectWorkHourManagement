define(function(require, exports, module) {
	module.exports = function() { /* Controllers */
		'use strict';
		var app = angular.module('ProjectManage.controllers');
		app.controller('mystaffsCtrl', ['$rootScope', '$scope', '$location', '$http', 'myStaffsFunc',
			function($rootScope, $scope, $location, $http, myStaffsFunc) {
				$scope.selectedCount = 0;

				function GetSelectedStaffsCount() {
					if ($rootScope.mystaffs == undefined) return 0;
					var count = 0;
					angular.forEach($rootScope.mystaffs, function(staff, key) {
						// body...
						if (staff.checked === true) {
							count++;

						}
					})
					$scope.selectedCount = count;
				}
				//获取我的收藏员工
				myStaffsFunc.getMyStaffs(GetSelectedStaffsCount);


				$scope.staffChecked = function(staff) {
					staff.checked = !staff.checked;
					if (staff.checked === true) {
						$scope.selectedCount++;
					} else {
						$scope.selectedCount--;
					}
				};


				$scope.doProject = function() {
					var array = $rootScope.mystaffs.sort($rootScope.SortStaffByChecked);
					$rootScope.todostaffs = [];
					for (var i = 0; i < $scope.selectedCount; i++) {
						array[i].WorkType = '监护长夜班';
						$rootScope.todostaffs.push(array[i]);
					}

					var count = $rootScope.todoprojects.length;
					//有选择工程
					if (count > 0) {
						localStorage.setItem("todostaffs", JSON.stringify($rootScope.todostaffs));
						localStorage.setItem("todoprojects", JSON.stringify($rootScope.todoprojects));
						$location.path('/todoproject').search({
							'Index': 0
						});
					} else {
						$location.path('/myprojects');
					}
				}
				$scope.editstaff = function() {
					for (var i = 0; i < $rootScope.mystaffs.length; i++) {
						if ($rootScope.mystaffs[i].checked === true) {
							var prj = $rootScope.mystaffs[i];
							localStorage.setItem('staff', JSON.stringify(prj));
							$location.path('/addstaff');
							break;
						}
					}
				}
				$scope.deleteStaffs = function() {
					var toBeDelete = [];
					for (var i = $rootScope.mystaffs.length - 1; i >= 0; i--) {
						if ($rootScope.mystaffs[i].checked == true) {
							toBeDelete.push($rootScope.mystaffs[i].Name);
							$rootScope.mystaffs.splice(i, 1);
							$scope.selectedCount--;
						}
					}
					myStaffsFunc.removeFavorStaffs(toBeDelete);

				}
				$scope.selectAll = function() {
					$scope.selectedCount = 0;
					angular.forEach($rootScope.mystaffs, function(staff, index) {
						staff.checked = true;
						$scope.selectedCount++;
					});

				};
			}
		]);
	};
});