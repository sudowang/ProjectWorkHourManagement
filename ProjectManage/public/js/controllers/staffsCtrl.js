define(function(require, exports, module) {
	module.exports = function() {
		var app = angular.module('ProjectManage.controllers');
		app.controller('staffsCtrl', ['$scope', '$http', '$timeout', '$rootScope', '$filter', 'allStaffsFunc', 'myStaffsFunc',
			function($scope, $http, $timeout, $rootScope, $filter, allStaffsFunc, myStaffsFunc) {

				$scope.allStaffs = [];
				$scope.filtered = [];
				$scope.paged = [];

				$scope.totalItems = 1;
				$scope.itemsPerPage = 10;
				$scope.currentPage = 1;

				var toBeAdd = [];
				var toBeDelete = [];


				$scope.pageChanged = function(page) {
					// body...
					$scope.paged = $scope.filtered.slice($scope.itemsPerPage * (page - 1), $scope.itemsPerPage * (page - 1) + $scope.itemsPerPage);
					$scope.currentPage = page;

				}

				function GetStaffAll() {
					allStaffsFunc.getAllStaffs(function() {
						$scope.allStaffs = $rootScope.staffs;
						$scope.$watch('searchTxt', function(newValue) {
							$scope.filtered = $filter('filter')($scope.allStaffs, $scope.searchTxt);
							$scope.totalItems = $scope.filtered.length;
							$scope.paged = $scope.filtered.slice(0, $scope.itemsPerPage);
							$scope.currentPage = 1;
						});
						$scope.searchTxt = $rootScope.user.name;
					});
				}
				$timeout(GetStaffAll, 900);

				$scope.clickFavor = function(staff) {
					staff.isFavor = !staff.isFavor;
					if (staff.isFavor === true) {
						var index = toBeDelete.indexOf(staff.Name);
						if (index > -1) {
							toBeDelete.splice(index, 1);
						} else {
							toBeAdd.push(staff.Name);
						}
						myStaffsFunc.addMyStaff(staff);
					} else {
						var index = toBeAdd.indexOf(staff.Name);
						if (index > -1) {
							toBeAdd.splice(index, 1);
						} else {
							toBeDelete.push(staff.Name);
						}
						myStaffsFunc.removeMyStaff(staff);
					}
				};

				//离开页面时,ajax数据库,增删操作
				$scope.$on("$destroy", function() {
					myStaffsFunc.addFavorStaffs(toBeAdd);
					myStaffsFunc.removeFavorStaffs(toBeDelete);
				});
			}
		]);

	}
});