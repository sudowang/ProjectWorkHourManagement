define(function(require, exports, module) {
	module.exports = function() {
		var app = angular.module('ProjectManage.controllers');
		app.controller('addprojectCtrl', ['$rootScope', '$scope', '$location', '$http', 'allProjectsFunc',
			function($rootScope, $scope, $location, $http, allProjectsFunc) {
				$scope.prj = JSON.parse(localStorage.getItem("prj"));
				var IsAdd = false;
				if ($scope.prj.PrjId === undefined) {
					IsAdd = true;
				}

				var oldPrj = angular.copy($scope.prj);


				$scope.nextStep = function() {
					var data = {
						'IsAdd': IsAdd,
						'OldValue': oldPrj,
						'NewValue': $scope.prj,
						'name': $rootScope.user.name
					};
					allProjectsFunc.addProject(data, callback);
				}

				function callback() {
					//当更改完成后发生
					localStorage.setItem('prj', "");
					if (IsAdd === true) {
						if ($rootScope.projects.length > 0) {
							$rootScope.projects.push($scope.prj);
						}
						if ($rootScope.myprojects.length > 0) {
							$rootScope.myprojects.push($scope.prj);
						}
					} else {
						for (var i = 0; i < $rootScope.myprojects.length; i++) {
							if ($rootScope.myprojects[i].checked === true) {
								$rootScope.myprojects[i] = $scope.prj;
							}
						}
					}
					$location.path("/home");
				}
			}
		]);
	}
});