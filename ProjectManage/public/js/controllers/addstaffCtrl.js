define(function(require, exports, module) {
	module.exports = function() {
		var app = angular.module('ProjectManage.controllers');
		app.controller('addstaffCtrl', ['$rootScope', '$scope', '$location', '$http', 'allStaffsFunc',
			function($rootScope, $scope, $location, $http, allStaffsFunc) {
				$scope.staff = JSON.parse(localStorage.getItem("staff"));
				var IsAdd = false;
				if ($scope.staff.Name === undefined) {
					IsAdd = true;
				}

				var oldValue = angular.copy($scope.staff);


				$scope.nextStep = function() {
					var data = {
						'IsAdd': IsAdd,
						'OldValue': oldValue,
						'NewValue': $scope.staff,
						'name': $rootScope.user.name
					};
					allStaffsFunc.addStaff(data, callback);
				}

				function callback() {
					//当更改完成后发生
					localStorage.setItem('staff', "");
					if (IsAdd === true) {
						if ($rootScope.staffs.length > 0) {
							$rootScope.staffs.push($scope.staff);
						}
						if ($rootScope.mystaffs.length > 0) {
							$rootScope.mystaffs.push($scope.staff);
						}
					} else {
						for (var i = 0; i < $rootScope.mystaffs.length; i++) {
							if ($rootScope.mystaffs[i].checked === true) {
								$rootScope.mystaffs[i] = $scope.staff;
							}
						}
					}
					$location.path("/mystaffs");
				}
			}
		]);
	}
});