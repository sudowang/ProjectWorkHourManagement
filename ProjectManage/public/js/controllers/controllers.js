define(function(require, exports, module) {
	module.exports = function() {
		angular.module('ProjectManage.controllers', []);
		require('./homeCtrl.js')();
		require('./indexCtrl.js')();
		require('./loginCtrl.js')();
		require('./projectsCtrl.js')();
		require('./todoprojectCtrl.js')();
		require('./todostaffCtrl.js')();
		require('./staffsCtrl.js')();
		require('./mystaffsCtrl.js')();
		require('./successCtrl.js')();
		require('./addprojectCtrl.js')();
		require('./addstaffCtrl.js')();
	}
});