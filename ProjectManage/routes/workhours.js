exports.postWorkHours = function(req, res) {
    var request = require('request');
    request({
        method: 'POST',
        url: global.apiUrl + '/workhours/',
        json: JSON.stringify(req.body)
    }, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            res.send(body);
        } else {
            res.send(false);
        }
    });
};
