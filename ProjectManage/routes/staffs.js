exports.getStaffs = function(req, res) {
	var api = global.apiUrl + '/staffs/';
	var request = require('request');
	request({
		method: 'get',
		url: api
	}, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			res.send(JSON.parse(body));
		} else {
			res.send('fail to load!');
		}
	})
}

exports.putStaff = function(req, res) {
	var api = global.apiUrl + '/staffs';
	var request = require('request');
	request({
		method: 'PUT',
		url: api,
		json: JSON.stringify(req.body)
	}, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			res.send(body);
		} else {
			res.send(false);
		}
	});

}